# Wallpapers
***
This repo was created to keep my wallpapers in version control and synced across machines. There's no License file, since I don't hold copyright to the art. But I try to keep track of the sources, linked in the subheadings. If you have a copyright complaint, create an issue and I'll take the image down.

A great deal of the images were upscaled with waifu2x, for future proofing in preparation for who-knows-how-many-K displays. And the .jpg files are exported **once** at 90% quality, to save on a bit of disk space. If you want the source quality, the links are there for most of them.

## Sorting
***
I put stuff into generalized folders. I only take a quick guess when deciding, so don't get angry.

### anime
This is where anime-girls, or anime related artwork live.

### archive
Used to have this for obsolete wallpapers.

### art-3drender
CG generated thingies.

### art-bitmap
Bitmap based digital art.

### art-vector
Vector based digital art.

### games
Either direct screenshots or otherwise vidya related art.

### landscapes
/r/outside

### photos
Doesn't need to be /r/outside, but it can appear here! So, mostly urban photography or portraits.

## Credits
***

### 5555 96 | [source](https://www.pixiv.net/member.php?id=20228000)
```
ark-order-5555-96.jpg
```

### aenami | [source](https://www.deviantart.com/aenami/gallery/)
```
endless-aenami.jpg
silence-aenami.jpg
```

### as109 | [source](https://www.pixiv.net/member_illust.php?illust_id=62518077&mode=medium)

Careful with that link. Other works of the artist are really disturbing nsfw material. This is the one exception where I linked directly only to that piece of art, instead of the profile/gallery.
```
motoko-kusanagi-as109.jpg
```

### Aurahack | [source](https://tightenupthe.graphics/)
```
shuten-douji-aurahack.jpg
```

### Azomo | [source](https://www.pixiv.net/member.php?id=6342480)
```
azomo-phos-shinsha.jpg
```

### Badriel | [source](https://www.pixiv.net/member.php?id=6928972)
```
badriel-itsukishima-torii.png
```

### Chen lan | [source](https://www.pixiv.net/member.php?id=5708814)
```
chen-lan-streetmoe.jpg
chen-lan-streetmoe.jpg
```

### Chuyen | [source](https://www.deviantart.com/chuyenn)
```
nanniimo-chuyen.jpg
```

### CJMY | [source](https://www.pixiv.net/member.php?id=24845859)
```
cjmy-fate-in-jazz.jpg
```

### /u/ClaudeVanFoxbat | [source](https://www.reddit.com/r/cowboybebop/comments/5cw1ik/i_made_a_faye_wallpaper_1920x1080_3840x2160_and/)
```
faye-valentine-u-claudevanfoxbat.png
```

### /u/Conpatshe | [source](https://old.reddit.com/r/classicwow/comments/c5ansd/for_those_prepping_their_desktops_for_classic/es0yvw1/)
```
ironforge-recolor-conpatshe.jpg
```

### Craig Drake | [source](http://craigdrakeart.com/)
```
delorean-blu-craig-drake.jpg
delorean-oran-craig-drake.jpg
terminator-craig-drake.png
```

### Denis Istomin | [source](https://www.artstation.com/gydwin)
```
denis-istomin-quiet.jpg
denis-istomin-tokyo-3.png
denis-istomin-your-heart.jpg
```

### daye bie qia lian | [source](https://www.pixiv.net/member.php?id=11578499)
```
asuka-daye-bie-qia-lian.jpg
rikka-akane-daye-bie-qia-lian.jpg
```

### Fabian Rensch | [source](https://www.artstation.com/fabianrensch)
```
fabian-rensch-breathofthewild.jpg
```

### Fedora | [source](https://fedoraproject.org/wiki/Wallpapers)
```
fedora-29.jpg
fedora-29-preview.png
```

### Haizome Senri [source](https://www.pixiv.net/member.php?id=9120483)
```
mistfog-haizome-senri.jpg
```

### J Adsen [source](https://www.pixiv.net/member.php?id=12059921)
```
satsuki-j-adsen.jpg
zero-two-j-adsen.jpg
```

### Joel Jurion | [source](https://www.pixiv.net/member.php?id=4587609)
```
motoko-joel-jurion.jpg
```

### JJune | [source](https://www.pixiv.net/member.php?id=1827932)
```
kanade-hayami-jjune.jpg
```

### katsuoboshi | [source](https://www.pixiv.net/member.php?id=1556509)
```
motoko-beach-katsuoboshi.jpg
motoko-freezing-katsuoboshi.jpg
motoko-profile-katsuoboshi.jpg
```

### kengo | [source](https://twitter.com/kengo1212)
```
office-ryuko.png
```

### Kuzinskiy | [source](https://www.deviantart.com/kuzinskiy/)
```
grommash-kuzinskiy.jpg
```

### Kogecha | [source](https://www.pixiv.net/member.php?id=12845810)
```
trabant-kogecha.jpg
```

### László Szabados | [source](https://laszloszabadosart.com/)
```
thunderbluff-laszlo-szabados.jpg
```

### LM7 | [source](https://www.pixiv.net/member.php?id=420928)
```
kirin-lm7-op-center.jpg
```

### /u/LogicWavelength | [source](https://www.reddit.com/r/wallpaper/comments/a2ootu/retro_wave_pyramid_oc_3840x2160/)
```
retrowave-pyramid-1.jpg
retrowave-pyramid-2.jpg
```

### Marek Denko | [source](https://www.artstation.com/denko)
```
marek-denko-hesitation.jpg
```

### Mikael Gustafsson | [source](https://www.artstation.com/mikaelgustafsson)
```
gustafsson-among-trees.jpg
gustafsson-small-memory.jpg
```

### monq | [source](https://www.pixiv.net/en/users/1167441)
```
monq-firefighter.jpg
monq-snuggle-bear.jpg
```

### nikusenpai | [source](https://www.pixiv.net/member.php?id=20213610)


### Official Art
Artwork that is officialy related to the property referenced in the list.
```
garden-of-words.jpg | Garden of Words 2013 Movie by Makoto Shinkai
grommash-wei-wang.jpg | From Warcraft franchise by Blizzard Entertainment. Artist presumed to be Wei Wang
kill-la-kill-key.jpg | Key Visual from Kill La Kill, 2014 Anime by Studio Trigger
motoko-official-95.jpg | Ghost in the Shell 1995 Movie by Mamoru Oshii
yurucamp-key-visual.jpg | Official Key Visual for the 2018 Anime Yuru Camp
```

### okianyumi | [source](https://www.behance.net/okianyumi)
```
okianyumi-daily-aug.jpg
```

### omutatsu | [source](https://www.pixiv.net/member.php?id=1499614)
```
omutatsu-umbrella.jpg
```

### Park JunKyu [source](https://www.artstation.com/gharly)
```
park-junkyu-dramaturgy.jpg
park-junkyu-hallucination.jpg
park-junkyu-marie.jpg
park-junkyu-print1.jpg
park-junkyu-print2.jpg
```

### Position Dream | [source](https://www.positrondream.com/2018)
```
three-wishes.png
```

### puppeteer777 | [source](https://www.pixiv.net/member.php?id=182737)
```
heart-puppeteer777.jpg
```

### racer | [racer](https://www.pixiv.net/member.php?id=4563141)
```
arknights-racer.jpg
```

### rshow | [source](https://www.pixiv.net/member.php?id=14297543)
```
nino-nakano-rshow.jpg
```


### Rui Li | [source](https://melodyoflost.artstation.com/)
```
the-glaxy-rui-li.jpg
```

### Seok Min Eo | [source](https://www.artstation.com/user-564)
```
seok-min-eo-mushroom-tree.jpg
```

### Sakurachen | [source](https://dribbble.com/SakuraChen)
```
sakurachen-aurora-forest.jpg
```

### Star Wars Battlefront | [source](https://en.wikipedia.org/wiki/Star_Wars%3A_Battlefront)
```
vader-battlefront.jpg
```

### SteamyTomato (justin-leyva) | [source](https://www.pixiv.net/member.php?id=9011357)
```
rikka-justin-leyva.jpg
```

### TAiGA | [source](https://www.pixiv.net/member.php?id=4810922)
```
drenched-in-blue-taiga.jpg
```

### TamplierPainter | [source](https://tamplierpainter.artstation.com/)
```
onyxia-classic-blizz
ragnaros-classic-tamplierpainter
```

### /u/timbin1 [source](https://old.reddit.com/r/wallpapers/comments/bz40wu/a_recent_edit_of_mine_inspired_by_witchoria/)
```
witchoria-timbin1.jpg
```

### vinneart | [source](https://twitter.com/vinneart)
```
streetmoe-squad-vinne.jpg
```

### vofan | [source](https://www.pixiv.net/en/users/51536)
```
vofan-ai-chan-1.jpg
vofan-ai-chan-2.jpg
vofan-ai-chan-3.jpg
vofan-city-under-sea.jpg
vofan-oshino-ougi.jpg
```

### wukloo | [source](https://www.pixiv.net/member.php?id=4401153)
```
wukloo-kiss-shot.jpg
```

### Yoneyama Mai | [source](https://www.pixiv.net/member.php?id=1554775)
```
esc.004-yoneyama-mai.jpg
esc.yori-yoneyama-mai.jpg
the-one-yoneyama-mai.jpg
```

### Yoshinari You | [source](https://www.animenewsnetwork.com/encyclopedia/people.php?id=3702)
```
evangelion-yoshinari-you.jpg
```

### Yulin Li | [source](https://www.artstation.com/liyulin)
```
yulin-li-161014.jpg
```

### No or only indirect source available
```
cosmo.png (https://www.deviantart.com/mahm0udwally/art/Cosmo-for-8-1-532873125)
osaka-rain.jpg (https://www.reddit.com/r/wallpapers/comments/a1imbk/osaka_under_the_rain_3276x2184/)
into-the-black-hole.jpg (https://old.reddit.com/r/wallpapers/comments/bywbzl/into_the_black_hole_1920x1080/)
fractal-texture-whvn.jpg (https://wallhaven.cc/w/p83wj9)
retrowave-pyramid-3.jpg (https://wallhaven.cc/w/p8yygp)
watchtower.png
```
